package com.basic.service;

import com.basic.model.Letter;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;

public class D implements AlphabetLetter{

    @PostConstruct
    public void init(){
        System.err.println("Inside @PostConstruct of D");
    }

    @Override
    public Letter getLetterInformation() {
        return new Letter("Une jam Karakteri D",4);
    }

    @PreDestroy
    public void destroy(){
        System.err.println("Inside @PreDestroy of D");
    }
}

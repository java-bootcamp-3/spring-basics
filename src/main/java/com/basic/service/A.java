package com.basic.service;

import com.basic.model.Letter;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.stereotype.Component;

@Component
public class A implements AlphabetLetter{

    @PostConstruct
    public void init(){
        System.err.println("Inside @PostConstruct of A");
    }

    @Override
    public Letter getLetterInformation() {
        return new Letter("Une jam Karakteri A",1);
    }

    @PreDestroy
    public void destroy(){
        System.err.println("Inside @PreDestroy of A");
    }
}

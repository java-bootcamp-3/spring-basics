package com.basic.service;

import com.basic.model.Letter;

public interface AlphabetLetter {
    Letter getLetterInformation();
}

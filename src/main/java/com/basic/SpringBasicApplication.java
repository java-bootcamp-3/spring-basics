package com.basic;

import com.basic.model.Letter;
import com.basic.service.A;
import com.basic.service.B;
import com.basic.service.C;
import com.basic.service.D;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBasicApplication implements CommandLineRunner {

	private final A a;
	private final B b;
	private final C c;
	private final D d;

	public SpringBasicApplication(A a, B b, C c, D d) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBasicApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Letter aLetter = a.getLetterInformation();
		Letter bLetter = b.getLetterInformation();
		Letter cLetter = c.getLetterInformation();
		Letter dLetter = d.getLetterInformation();
		System.out.println(String.format("Message: %s , Postion %s",aLetter.getInfo(),aLetter.getPosition()));
		System.out.println(String.format("Message: %s , Postion %s",bLetter.getInfo(),aLetter.getPosition()));
		System.out.println(String.format("Message: %s , Postion %s",cLetter.getInfo(),aLetter.getPosition()));
		System.out.println(String.format("Message: %s , Postion %s",dLetter.getInfo(),aLetter.getPosition()));
	}
}

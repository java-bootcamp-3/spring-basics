package com.basic.repository;

import com.basic.model.Address;
import com.basic.model.User;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class UserRepository {
    List<User> users;

    @PostConstruct
    public void init(){
        var address1 = new Address("Address 1","1000");
        var address2 = new Address("Address 2","1001");
        var user1 = new User(1,"User 1",address1);
        var user2 = new User(2,"User 2",address1);
        var user3 = new User(3,"User 3",address1);
        var user4 = new User(4,"User 4",address2);
        var user5 = new User(5,"User 5",address2);
        var user6 = new User(6,"User 6",address2);
        users = Arrays.asList(user1,user2,user3,user4,user5,user6);
    }

    public User findUserById(Integer id){
        return users.stream().filter(u -> u.getId()==id)
                .findFirst().orElse(null);
    }

    public List<User> findUsersByZipCode(String zipCode){
        return users.stream().filter(u -> u.getAddress().getZipCode().equals(zipCode))
                .collect(Collectors.toList());
    }
}

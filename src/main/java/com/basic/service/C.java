package com.basic.service;

import com.basic.model.Letter;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;

public class C implements AlphabetLetter{

    @PostConstruct
    public void init(){
        System.err.println("Inside @PostConstruct of C");
    }

    @Override
    public Letter getLetterInformation() {
        return new Letter("Une jam Karakteri C",3);
    }

    @PreDestroy
    public void destroy(){
        System.err.println("Inside @PreDestroy of C");
    }
}

package com.basic.model;

public class Letter {
    private String info;
    private Integer position;

    public Letter(String info, Integer position) {
        this.info = info;
        this.position = position;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }
}

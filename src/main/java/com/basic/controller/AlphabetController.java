package com.basic.controller;

import com.basic.model.Letter;
import com.basic.service.A;
import com.basic.service.B;
import com.basic.service.C;
import com.basic.service.D;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AlphabetController {

    @Autowired
    private  A a;
    @Autowired
    private  B b;
    @Autowired
    private  C c;
    @Autowired
    private  D d;

    @GetMapping("/api/letter-info/{letter}")
    public Letter getLetterInformation(@PathVariable(name = "letter") String l){
        return switch (l){
           case "A"-> a.getLetterInformation();
           case "B"-> b.getLetterInformation();
           case "C"-> c.getLetterInformation();
           case "D"-> d.getLetterInformation();
           default -> new Letter("undefined",-1);
        };
    }




}

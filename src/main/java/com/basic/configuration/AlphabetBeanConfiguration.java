package com.basic.configuration;

import com.basic.service.B;
import com.basic.service.C;
import com.basic.service.D;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AlphabetBeanConfiguration {

    @Bean
    public C initC(){
        return new C();
    }

    @Bean
    public D initB(){
        return new D();
    }
}
